"""Seed the Mozzarella database"""
import transaction
from mozzarella import model


def bootstrap(command, conf, vars):
    # <websetup.bootstrap.before.auth>
    from sqlalchemy.exc import IntegrityError
    try:
        david = model.User(
            user_id=1984,
            user_name="davidflorness",
            display_name="David Florness")
        model.DBSession.add(david)

        g = model.Group()
        g.group_name = 'officers'
        g.display_name = 'Officers'

        g.users.extend([david])
        model.DBSession.add(g)

        p = model.Permission()
        p.permission_name = 'admin'
        p.description = 'This permission gives an administrative right'
        p.groups.append(g)
        model.DBSession.add(p)

        model.DBSession.add(model.OfficerTitle(title='President', rank=0))
        model.DBSession.add(model.OfficerTitle(title='Vice President', rank=1))
        model.DBSession.add(model.OfficerTitle(title='Secretary', rank=2))
        model.DBSession.add(model.OfficerTitle(title='Treasurer', rank=3))
        model.DBSession.flush()

        transaction.commit()
    except IntegrityError:
        print('Warning, there was a problem adding your data, '
              'it may have already been added:')
        import traceback
        print(traceback.format_exc())
        transaction.abort()
        print('Continuing with bootstrapping...')

    # <websetup.bootstrap.after.auth>
