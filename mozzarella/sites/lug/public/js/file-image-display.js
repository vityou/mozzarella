$(document).ready(function() {

    /* For the image preview */
    $(":file").change(function () {
        if (this.files && this.files[0]) {
            ImageTools.resize(this.files[0], {width: 512, height: 512},
                    function(blob, resized) {
                        var reader = new FileReader();
                        reader.onload = function (res) {
                            $("#before-select").hide();
                            $("#after-select").fadeIn();
                            $("#after-select-img").attr('src', res.target.result);
                        };
                        reader.readAsDataURL(blob);
                    }
            );
        }
    });

    /* For the upload button */
    $('#upload-button').click(function (event) {
        event.preventDefault();
        $.post("/pref/image_set", {
            img: $("#after-select-img").attr('src')
        }, function(goto_url) {
            window.location = goto_url;
        },
        "text");
    });
});
