(function($){
    var last_query = "";
    $("#ml-username").on("change keyup paste", function(){
        if ($("#ml-username").val().indexOf("@") >= 0) {
            $("#ml-username").val($("#ml-username").val().replace(/\@.*$/g, ""));
            alert("Only a Mines username is required, not a full email address.");
        }
        if ($("#ml-username").val() != last_query && $("#ml-username").val().length > 1) {
            last_query = $("#ml-username").val();
            $.get("http://toilers.mines.edu/toilers-cgi-bin/namequery.cgi?un="+$("#ml-username").val(), function(data) {
                if (data[0] != "?") {
                    $("#ml-fullname").val(data.trim());
                    $(".ml-fullname-warn").fadeIn();
                }
            });
        }
    });

    $('form').submit(function() {
        $(this).find("button[type='submit']").prop('disabled',true);
    });

})(jQuery);
